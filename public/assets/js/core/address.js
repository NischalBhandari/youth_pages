$(function(){

    $.fn.addressTriggers = function (options) {
        let defaults = $.extend({
            state: null,
            city: null,
            add1: null
        }, options);

        let stateObj,cityObj,addr1Obj = null;
        let countryObj = this;
        this.select2({});

        let refreshStateObj = (obj) => {
            obj.select2({
                placeholder: `-- Select ${defaults.state.placeholder} --`,
                language: {
                    noResults: function(){
                        let term = obj.data('select2').dropdown.$search.val();
                        let noResultWrap = $('<div></div>');

                        if(term){
                            noResultWrap.append($(`<p><strong>${term ? term : 'Result'}</strong> not found.</p>`));
                            let link = $(`<a>Add ${term ? term : 'new'}</a>`);
                            link.on('click', function(e){
                                link.prop('disabled', true);
                                $.ajax({
                                    type: 'post',
                                    url: defaults.state.add,
                                    data: {country: countryObj.val(), name: term},
                                    success: function(res){
                                        obj.append(`<option value="${res.data.id}">${res.data.name}</option>`)
                                        obj.val(res.data.id);
                                        refreshStateObj(obj);
                                        triggerStateChange(res.data.id);
                                    },
                                    error: function(){}
                                });
                            });
                            noResultWrap.append(link);
                        }

                        return noResultWrap;
                    }
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });
        };

        let refreshCityObj = (obj) => {
            obj.select2({
                placeholder: `-- Select ${defaults.city.placeholder} --`,
                language: {
                    noResults: function(){
                        let term = obj.data('select2').dropdown.$search.val();
                        let noResultWrap = $('<div></div>');
                        let stateId = stateObj.val() || null;

                        noResultWrap.append($(`<p class="dWarn">${stateId ? 'No result found.' : defaults.state.placeholder + ' not selected'}</p>`));

                        if(stateId && term){
                            noResultWrap.html('');
                            noResultWrap.append($(`<p><strong>${term}</strong> not found.</p>`));
                            let link = $(`<a>Add ${term ? term : 'new'}</a>`);
                            link.on('click', function(e){
                                link.prop('disabled', true);
                                $.ajax({
                                    type: 'post',
                                    url: defaults.city.add,
                                    data: {province: stateId, name: term},
                                    success: function(res){
                                        obj.append(`<option value="${res.data.id}">${res.data.name}</option>`);
                                        obj.val(res.data.id);
                                        refreshCityObj(obj);
                                        triggerCityChange(res.data.id);
                                    },
                                    error: function(){}
                                });
                            });
                            noResultWrap.append(link);
                        }

                        return noResultWrap;
                    }
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });
        };

        let refreshAdd1Obj = (obj) => {
            obj.select2({
                placeholder: `-- Select ${defaults.add1.placeholder} --`,
                language: {
                    noResults: function(){
                        let term = obj.data('select2').dropdown.$search.val();
                        let noResultWrap = $('<div></div>');
                        let cityId = cityObj.val() || null;
                        noResultWrap.append($(`<p class="dWarn">${cityId ? 'No result found.' : defaults.city.placeholder + ' not selected'}</p>`));

                        if(cityId && term){
                            noResultWrap.html('');
                            noResultWrap.append($(`<p><strong>${term ? term : 'Result'}</strong> not found.</p>`));
                            let link = $(`<a>Add ${term ? term : 'new'}</a>`);
                            link.on('click', function(e){
                                link.prop('disabled', true);
                                $.ajax({
                                    type: 'post',
                                    url: defaults.add1.add,
                                    data: {city: cityObj.val(), name: term},
                                    success: function(res){
                                        obj.append(`<option value="${res.data.id}">${res.data.name}</option>`)
                                        obj.val(res.data.id);
                                        refreshAdd1Obj(obj);
                                    },
                                    error: function(){}
                                });
                            });
                            noResultWrap.append(link);
                        }

                        return noResultWrap;
                    }
                },
                escapeMarkup: function (markup) {
                    return markup;
                }
            });
        };

        if(defaults.state !== null){
            stateObj =  $(defaults.state.element);
            refreshStateObj(stateObj);
        }

        if(defaults.city !== null){
            cityObj =  $(defaults.city.element);
            refreshCityObj(cityObj);
        }

        if(defaults.add1 !== null){
            addr1Obj =  $(defaults.add1.element);
            refreshAdd1Obj(addr1Obj);
        }

        let stateGetRequest = null;
        let cityGetRequest = null;
        let add1GetRequest = null;

        let triggerCountryChange = (id) => {
            if(stateObj){
                let emptyEle = stateObj.find('option[value=""]');
                stateObj.html('').append(emptyEle);

                stateObj.on('change', function(e){
                    let _self = $(this);
                    triggerStateChange(_self.val());
                });
                stateGetRequest = $.ajax({
                    type: 'get',
                    url: defaults.state.get,
                    data: {country: id},
                    beforeSend: function(){ if(stateGetRequest != null){ stateGetRequest.abort(); }},
                    success: function(res){
                        $.each(res.data, function(i,v){
                            stateObj.append(`<option value="${i}">${v}</option>`)
                        });
                        stateObj.val(defaults.state.selected);
                        refreshStateObj(stateObj);
                        triggerStateChange(defaults.state.selected);
                    },
                    error: function(res){return [];}
                });
            }
        };

        let triggerStateChange = (id) => {
            if(cityObj){
                let emptyEle = cityObj.find('option[value=""]');
                cityObj.html('').append(emptyEle);
                cityObj.on('change', function(e){
                    let _self = $(this);
                    triggerCityChange(_self.val());
                });

                if(id){
                    cityGetRequest = $.ajax({
                        type: 'get',
                        url: defaults.city.get,
                        data: {province: id},
                        beforeSend: function(){ if(cityGetRequest != null){ cityGetRequest.abort(); }},
                        success: function(res){
                            $.each(res.data, function(i,v){
                                cityObj.append(`<option value="${i}">${v}</option>`)
                            });
                            cityObj.val(defaults.city.selected);
                            refreshCityObj(cityObj);
                            triggerCityChange(defaults.city.selected);
                        },
                        error: function(res){return [];}
                    });
                }else{
                    triggerCityChange(defaults.city.selected);
                }


            }
        };

        let triggerCityChange = (id) => {
            if(addr1Obj){
                let add1Obj = $(defaults.add1.element);
                let emptyEle = add1Obj.find('option[value=""]');
                add1Obj.html('').append(emptyEle);
                if(id){

                    add1GetRequest = $.ajax({
                        type: 'get',
                        url: defaults.add1.get,
                        data: {city: id},
                        beforeSend: function(){ if(add1GetRequest != null){ add1GetRequest.abort(); }},
                        success: function(res){
                            $.each(res.data, function(i,v){
                                add1Obj.append(`<option value="${i}">${v}</option>`)
                            });
                            add1Obj.val(defaults.add1.selected);
                            refreshAdd1Obj(addr1Obj);
                        },
                        error: function(res){return [];}
                    });
                }
            }
        };

        this.on('change', function(e){
            let _self = $(this);
            triggerCountryChange(_self.val());
        });

        if(this.val()){
            this.trigger('change')
        }

        return this;
    };

});
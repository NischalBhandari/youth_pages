$('.fnPrint').fnPrint({});

$('.fnActReportUpdate').fnAjaxModal({
    onGetSuccess: function(modal, res, _ele){
        let collectionHolder = modal.find('.files')
        let addFileBtn = modal.find('#fn-add-files');
        addFileBtn.on('click', function(e){
            e.preventDefault();
            let rowString = collectionHolder.data('prototype');
            let nextIndex = (collectionHolder.data('index') || 0) + 1;
            let newRow = $(rowString.replace(/__name__/g, nextIndex));
            collectionHolder.data('index', nextIndex);

            newRow.find('.fn-delete-row').on('click', function(){
                if(confirm('Remove file')){
                    newRow.remove();
                }
            });

            collectionHolder.append(newRow);
        });

        let remarksEle = modal.find('.fn-report-remarks');
        let str = remarksEle.clone().wrap('<div/>').parent().html();

        remarksEle.addClass('hidden');
        let ckeditor;
        ClassicEditor
            .create(str)
            .then( editor => {
                remarksEle.before(editor.ui.element);
                ckeditor = editor;
                editor.setData(remarksEle.html());
                editor.model.document.on( 'change:data', () => {
                    remarksEle.html( editor.getData() );
                } );
            })
            .catch( error => {
                console.error( error );
            } );


        modal.find('.fn-report-template').on('change', function(){
            let template = $(this).find('option:selected').data('template') || '';
            let patientName = $('.patientName').data('name');
            template = template.replace(/\__patientName/g,patientName);
            ckeditor.setData(template);
        });


    },
    onSuccess: function(res, _ele){
        if(res.removeRow && res.removeRow === true){
            _ele.closest('tr').remove();
        }

        if(res.reload && res.reload === true){
            location.reload();
        }
    }
});

let initPrescriptionForm = (_ele, _callback) => {
    _ele.fnAjaxModal({
        size: 'modal-lg',
        onGetSuccess: (modal, res, _target) => {
            let collectionHolder = modal.find('#fnCollectionHolder')
            let addMoreBtn = modal.find('#fnAddMoreButton');
            addMoreBtn.on('click', function(e){
                e.preventDefault();
                let rowString = collectionHolder.data('prototype');
                let nextIndex = (collectionHolder.data('index') || 0) + 1;
                let newRow = $(rowString.replace(/__name__/g, nextIndex));
                collectionHolder.data('index', nextIndex);

                newRow.find('.fnDeleteRow').on('click', function(){
                    if(confirm('Remove Row')){
                        newRow.remove();
                    }
                });

                newRow.find('select').select2();
                newRow.find(`.datePicker`).fnSingleDatepicker();
                collectionHolder.append(newRow);
            });
            if(!collectionHolder.find('.fnItemRow').length){
                addMoreBtn.trigger('click');
            }

        },
        onSuccess: (res, _target) => { _callback(res, _target); }
    });
};
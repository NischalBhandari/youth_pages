$(document).ready(function () {
// english to nepali
    $('.datePicker-from-ad').datetimepicker(
        {
            format: 'YYYY-MM-DD',
            useCurrent: false,
        }
    ).on('dp.change', function (event) {
        convertEngToNepFnc()
    });

    const convertEngToNepFnc = () => {
        let engDate = $(".datePicker-from-ad").val();

        let currentDate = new Date(engDate);
        let currentNepaliDate = calendarFunctions.getBsDateByAdDate(currentDate.getFullYear(), currentDate.getMonth() + 1, currentDate.getDate());
        let formatedNepaliDate = calendarFunctions.bsDateFormat("%y-%m-%d", currentNepaliDate.bsYear, currentNepaliDate.bsMonth, currentNepaliDate.bsDate);
        
        $(".datePicker-from-bs").val(formatedNepaliDate);

        var age = getAge(engDate);
        $('.age').val(age);


    }
    // english to nepali

    // nepali to english
    $(".datePicker-from-bs").nepaliDatePicker({
        dateFormat: "%y-%m-%d",
        closeOnDateSelect: true,
        onSelect: function(dateSelect) {
            console.log('dateSelect');
        }
    });

    $('.datePicker-from-bs').on('dateSelect', function(e) {

    let convertedAdDate = e.datePickerData.adDate;
    let finalEngDate = nepToEngByEvent(convertedAdDate);

        $(".datePicker-from-ad").val(finalEngDate);

        var age = getAge(finalEngDate);
        $('.age').val(age);
    });

    let nepToEngByEvent = (adDate) => {

        let newAdYear = adDate.getFullYear();
        let newAdMonth = adDate.getMonth() + 1; 
        let newAdDate = adDate.getDate();

        let engDateWithFormat = newAdYear+'-'+newAdMonth +'-'+newAdDate;

        return engDateWithFormat;
    }
    // nepali to english

    //age calculation
    function getAge(dateVal) {

        var
            birthday = new Date(dateVal),
            today = new Date(),
            ageInMilliseconds = new Date(today - birthday),
            years = ageInMilliseconds / (24 * 60 * 60 * 1000 * 365.25 ),
            months = 12 * (years % 1),
            days = Math.floor(30 * (months % 1));
//                return Math.floor(years) + ' years ' + Math.floor(months) + ' months ' + days + ' days';
        return Math.floor(years);

    }




});
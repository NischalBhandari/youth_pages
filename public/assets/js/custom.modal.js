(function ($) {
  $.fn.renderCustomModal = function (options) {
    var defaults = $.extend({
      url:'',
      title:'',
      reload: '',
      clickAction: function(evt) {},
      modalAppendedAction: function() {},
      successAction: function(res){},
      selectOptionAction:function (res) {},
      errorAction: function(res){},
    }, options);
    this.on('click', function (evt) {
      defaults.clickAction(evt);
      let modal = "<div class='modal fade custom-modal' id='customModal' role='dialog'>" +
          "<div class='modal-dialog'>" +
          "<div class='modal-content'>" +
          "<div class='modal-header bg-table-head'>" +
          "<button type='button' class='close' data-dismiss='modal'>&times;</button> "+
          "<h6 class='modal-title'>"+defaults.title+"</h6>"+
          "</div>"+
          "<div class='modal-body' id='detail'></div>"+
          "</div>"+
          "</div>"+
          "</div>";
      $('body').append(modal);
      defaults.modalAppendedAction();
      $.ajax({
        type:'get',
        url:defaults.url,
        data:{},
        success:function (res) {
          $('body').find("#customModal").find('.modal-body').html(res.template);
          $('body').find("#customModal").modal('show');
          defaults.selectOptionAction(res);
          $("#customModal").find('form').on('submit', function (e) {
            e.preventDefault;
            var form = $(this);
            var dataToSend = form.serialize();
            var url = defaults.url;
            $.ajax({
              type: "POST",
              url: url,
              data: dataToSend,
              success: function (response) {
                if (res.status == false) {
                  $('#customModal').find('.modal-body').html(response.status);
                } else {
                  if(defaults.reload==true){
                    window.location.reload();
                  }
                  defaults.successAction(response);
                  $('#customModal').modal('hide');
                }
              },
              error: function (res) {
                $('#customModal').find('.modal-body').html(res.status);
              }
            });
            return false;
          })
        },
        error: function (res) {
          defaults.errorAction(res);
          alert(res.message);
        }
      });
    });
    return this;
  }



}(jQuery));
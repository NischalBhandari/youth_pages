const blocks = [{name: "bed", desc: "Bed Billing"},{name:"consultation",desc:"Consultation Billing"},{name:"lab",desc:"Lab Billing"} ]
function BillingContainer(props) {
 
    const [loadedRecords, setLoadedRecords] = React.useState(false);
    const [records,setRecords] = React.useState([]);

    const [feeTopics,setFeeTopics] = React.useState([]);
    const [loadedTopics, setLoadedTopics] = React.useState(false);
    
    const [total,setTotal] = React.useState(0);

    const [options,setOptions] = React.useState([]);
    const [loadedOptions,setLoadedOptions] = React.useState(false);
    const [rowAdded,setRowAdded] = React.useState(false);

    let otherRecordTemp = [];
    let optionsTemp = [];
    
    let newRec;

    const admissionId = props.admissionId;
    async function fetchRecs (){ 
        const result = await fetch("/ipd/admission/"+admissionId+"/api/admission/details");
        const json = await result.json();
        if (json.status==true){
            setRecords(json.wardRecArr);
        }
        setLoadedRecords(true);
    }

    async function fetchFeeTopics (){ 
        const result = await fetch("/ipd/billing/"+admissionId+"/fee-topic/list");
        const json = await result.json();
        if (json.status==true){
            setFeeTopics(json.feeTopicArr);
        }
        setLoadedTopics(true);
    }
    
    function calcTotal(){
        let totTemp=0;
        if (records.length>0)
        { for (let i=0;i<records.length;i++)
          { if (records[i].bill==true) { 
            totTemp = totTemp+records[i].rowTotal;
           } }
        }
        setTotal(totTemp);
    }
  

    React.useEffect( ()  => {
        if (!loadedRecords){
            fetchRecs(); 
        }
        if (!loadedTopics){
            fetchFeeTopics(); 
        }

        if (loadedTopics && !loadedOptions){
            for (let i=0;i<feeTopics.length;i++){
                optionsTemp[i] = {value: feeTopics[i].id,label: feeTopics[i].name}
            }
            setLoadedOptions(true);
            setOptions(optionsTemp);
        }
         if (rowAdded){
            otherRecordTemp = records;
            newRec = {item:"",itemId:"",itemFeeName:"",quantity:0,rate:0,rowTotal:0,bill:true,type:'OTHERS'};
            otherRecordTemp.push(newRec);
            setRecords(otherRecordTemp);
            setRowAdded(false);
         }
         calcTotal();
    })

   

        function addNewRow(){
        setRowAdded(true);
        }


       if (!loadedRecords && !loadedTopics){
           return (<div>Loading</div>);
       }

       else {
    
       return(
                <div>
                    <div className="row">
                    <button className="btn btn-primary pull-right" 
                    onClick={() => addNewRow()}
                    style={{
                        marginTop: -16 + 'px',
                        marginBottom: 3+'px',
                        marginRight: 6+'px',
                    }}
                    > Add More </button>
                    </div>
                    <RecordTable loadedRecords={loadedRecords} setLoadedRecords={setLoadedRecords}
                                  records={records} setRecords={setRecords} options={options}
                                  feeTopics={feeTopics} admissionId={admissionId}
                        total={total} setTotal={setTotal} calcTotal={()=>calcTotal()}
                    />
       
                </div>
        )
       }
}

function RecordTable(props){
    async function saveProduct (){ 
        fetch('/ipd/billing/create-invoice/'+props.admissionId+'/post', {
          method: 'post',
          headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(props.records)
        }).then(res => res.json())
          .then(res => {
            if (res.status==true)
            {console.log(res);
            }
          });
      };


    function submitInvoice(e){
        e.preventDefault();
        saveProduct();
    }
    return(<div>
          
                    <form>

                        <div className="row" style={{backgroundColor:"#e6e5e6"}}>
                            <div className="col-md-1"><strong>#</strong></div>
                            <div className="col-md-2"><strong>Item</strong></div>
                            <div className="col-md-2"><strong>Quantity</strong></div>
                            <div className="col-md-2"><strong>Rate</strong></div>
                            <div className="col-md-2"><strong>Particulars</strong></div>
                            <div className="col-md-2"><strong>Total</strong></div>
                            <div className="col-md-1"></div>
                        </div>
                         {props.records && props.records.map((rec,index) => 
                           {return(<BillingRowSelectTest key={index} chilIndex={index} rowItem={rec} rowVal ={index+1}
                            records={props.records} setRecords={props.setRecords}
                            feeTopics={props.feeTopics} options={props.options}
                            calcTotal={props.calcTotal}
                            />)}
                        )}                          
                         <div className="row">
                            <div className="col-md-7">
                            </div>
                            <div className="col-md-2">
                            <strong>Total</strong>    
                            </div>   
                            <div className="col-md-1">
                            {props.total}    
                            </div>   
                         </div>
                         <div className="row">

                            <button className="btn btn-primary pull-right" onClick={(e)=>submitInvoice(e)}>SAVE</button>   
                           
                         </div>
                        </form>
        </div>);
}

function BillingRowSelectTest(props) {

    const [quantity,setQuantity] = React.useState(props.rowItem.quantity);
    const [rate,setRate] = React.useState(props.rowItem.rate);
    const [bill,setBill] = React.useState(props.rowItem.bill);
    const [item,setItem] = React.useState(props.rowItem.item);
    const [itemId,setItemId] = React.useState(props.rowItem.itemId);
    const [itemFeeName,setItemFeeName] = React.useState(props.rowItem.itemFeeName);
    const [bgColour,setBgColour] = React.useState('#e6e5e6');
    let recTempHolder = props.records;
    let options = [];
    function handleQuantityChange(e){
        recTempHolder[props.chilIndex]['quantity']=e.target.value;
        recTempHolder[props.chilIndex]['rowTotal']=e.target.value*rate;
        setQuantity(e.target.value);
        props.setRecords(recTempHolder);
        props.calcTotal();

    };
    function handleRateChange(e){
        recTempHolder[props.chilIndex]['rate']=e.target.value;
        recTempHolder[props.chilIndex]['rowTotal']=e.target.value*quantity;
        setRate(e.target.value);
        props.setRecords(recTempHolder);
        props.calcTotal();

    };
    function handleCheckboxChange(e){
        recTempHolder[props.chilIndex]['bill']=e.target.checked;
        setBill(e.target.checked);
        props.setRecords(recTempHolder);
        props.calcTotal();
    }

    function handleSelectChange(e){
        setItemId(e.value);
        setItemFeeName(e.label);
        for (let i=0;i<props.feeTopics.length;i++)
        {   
            if (props.feeTopics[i].id == e.value){
            setRate(props.feeTopics[i].amount);
            recTempHolder[props.chilIndex]['rate']=props.feeTopics[i].amount;
            recTempHolder[props.chilIndex]['rowTotal']=props.feeTopics[i].amount*quantity;
            recTempHolder[props.chilIndex]['itemId']=e.value;
            recTempHolder[props.chilIndex]['itemFeeName']=e.label;
            }

        }
        props.setRecords(recTempHolder);
        props.calcTotal();
    }

    function handleParticularsChange(e){
        setItem(e.target.value);
        recTempHolder[props.chilIndex]['item']=e.target.value;
        props.setRecords(recTempHolder);
    }
    React.useEffect( () => {
        if (props.chilIndex%2==0){
            setBgColour('#FFF');
        }
    })
   

    return(<div key={props.chilIndex} className="row" style={{padding:2+"px",backgroundColor:bgColour}} > 
              <div className="col-md-1">
                  {props.rowVal}
              </div>
               <div className="col-md-2">
                  <Select defaultValue={{value:itemId,label:itemFeeName}} options={props.options} 
                   onChange={e => {handleSelectChange(e)}}
                  />
             </div>
             <div className="col-md-2">
                <input className="form-control" type="number" 
                value={quantity}
                onChange={(e)=>{handleQuantityChange(e)}}
                ></input>
              </div>
              <div className="col-md-2">
                <input className="form-control" type="number"
                 value={rate}
                 onChange={(e)=>{handleRateChange(e)}}
                 ></input>
              </div>
              <div className="col-md-2">
                 <input className="form-control" type="text"
                 value={item}
                 onChange={(e)=>{handleParticularsChange(e)}}>
                </input> 
              </div>
              <div className="col-md-2">
               {quantity*rate}
              </div>
              <div className="col-md-1">
              <input
                    checked={bill}
                    type="checkbox"
                    id={`pageblock-${props.index}`}
                    onChange={(e) => {handleCheckboxChange(e)}}
                />

              </div>
            </div>);
} 
<?php

namespace App\Admin\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;


class ButtonExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('fn_submit_button', [$this, 'fnSubmitButton'], ['is_safe' => ['html']]),
            new TwigFunction('fn_sidebar_link', [$this, 'renderSidebarLink'], ['is_safe' => ['html']]),
            new TwigFunction('fn_breadcrumb_button', [$this, 'fnBreadcrumbButton'], ['is_safe' => ['html']]),

        ];
    }

    public function fnSubmitButton(?string $extraClass = 'fnSb', ?string $label = 'Save'): string
    {
        return sprintf(
            '<button class="btn bg-Cornflower-400 submit-btn %s" name="save" type="submit"><i class="fa fa-save position-left"></i> &nbsp; %s</button>',
            $extraClass,
            $label
        );
    }

    public function fnBreadcrumbButton($permission, $route, $label, $attributes = []): string
    {
        if (!$this->hasAccess($permission)) return '';

        $iconEle = $this->renderIconElement($attributes['icon'] ?? '', true);
        $attributes['class'] = $attributes['class'] ?? 'btn-info';
        $extras = $this->getButtonAttributes($label, $attributes, 'btn position-left');

        return sprintf('<li><a tabindex="-1" href="%s"  %s>%s%s</a></li>', $route, $extras, $iconEle, $label);
    }

    public function hasAccess($permission): bool
    {
        return true;
    }

    public function renderIconElement($class = "", $showLabel = false): string
    {
        return $class ? sprintf('<i class="%s %s"></i> ', $class, ($showLabel ? 'position-left' : '')) : '';
    }

    private function getButtonAttributes($label, $attributes = [], $defaultClass = ''): string
    {
        $attributes['class'] = isset($attributes['class']) ? sprintf('%s %s', $defaultClass, $attributes['class']) : $defaultClass;
        $attributes['title'] = isset($others['title']) ? $attributes['title'] : $label;
        $attributes['data-original-title'] = isset($attributes['data-original-title']) ?? $label;

        $extras = '';
        foreach ($attributes as $k => $v) {
            $extras = sprintf('%s %s="%s"', $extras, $k, $v);
        }

        return $extras;
    }

    public function renderSideBarLink($permission, $route, $label, $attributes = []): string
    {
        if (!$this->hasAccess($permission)) return '';

        $iconEle = $this->renderIconElement($attributes['icon'] ?? '');
        $extras = $this->getButtonAttributes($label, $attributes, 'sidebar-link');
        $hiddenMenu = sprintf('<span class="hide-menu">%s</span>', $label);
        return sprintf('<li class="sidebar-item"><a tabindex="-1" href="%s" %s>%s%s</a>', $route, $extras, $iconEle, $hiddenMenu);
    }
}

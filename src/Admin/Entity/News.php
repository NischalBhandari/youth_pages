<?php

namespace App\Admin\Entity;

use App\Admin\Component\PostInterface;
use App\Admin\Repository\NewsRepository;
use Doctrine\ORM\Mapping as ORM;
use App\Admin\Entity\Post;

/**
 * @ORM\Entity(repositoryClass=NewsRepository::class)
 */
class News extends Post implements PostInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $content;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }
}

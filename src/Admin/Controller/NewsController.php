<?php


namespace App\Admin\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Admin\Entity\News;
use Symfony\Component\Routing\Annotation\Route;
use App\Admin\Form\NewsType;


class NewsController extends AbstractController
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return Response
     * @Route("/admin/news/list", name="app_news_list")
     */
    public function index(Request $request)
    {
        $data['newsList'] = $this->em->getRepository(News::class)->findAll();
        return $this->render('Admin/news/list.html.twig', $data);
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     * @Route("/admin/news/create",name="app_news_create")
     * @Route("/admin/news/update/{id}",name="app_news_update")
     */
    public function create(Request $request)
    {
        if ($request->get('id')) {
            $news = $this->em->getRepository(News::class)->find($request->get('id'));
        } else {
            $news = new News();
        }
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                try {
                    $this->em->persist($news);
                    $this->em->flush();
                    return $this->redirectToRoute('app_news_list');
                } catch (\Exception $e) {
                    return new \Exception("Cannot add the news");
                }
            }
        }
        $data['form'] = $form->createView();
        return $this->render('Admin/News/form.html.twig', $data);
    }
}
